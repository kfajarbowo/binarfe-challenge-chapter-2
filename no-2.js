const checkTypeNumber = (givenNumber) =>{
    // validation
    if(typeof givenNumber !== "number") {
        return "Error : Invalid type data"
    }else if(givenNumber === undefined){
        return "Error : Bro where is the parameter"
    }else{
        return givenNumber % 2 === 0 ? "Genap" : "Ganjil"
    }
    
}


console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber("3"))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())
