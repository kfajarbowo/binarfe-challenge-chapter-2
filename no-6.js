const dataNumber= [9,4,7,7,4,3,2,2,8]


// process
function getSecondLargeNumber(dataNumber){

//validation
if(dataNumber === undefined) return "Error function need an array parameter"

if(!Array.isArray(dataNumber)) return "Error : Invalid data type"

    firstLargestNumber = Math.max(...dataNumber) 
    index = dataNumber.indexOf(firstLargestNumber) //index dari firstLargestNumber
    dataNumber.splice(index, 1) // Delete first largest number
    secondLargestNumber = Math.max(...dataNumber) // firstlargestNumber ter remove, lanjut cari largest number
    return (secondLargestNumber) 
}

console.log(getSecondLargeNumber(dataNumber))
console.log(getSecondLargeNumber(0))
console.log(getSecondLargeNumber())
