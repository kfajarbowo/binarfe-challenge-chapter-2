function checkEmail(email){

    //validation
    if(email ===  undefined){
        return "Error : there is no email to check"
    }

    if(typeof email !== "string"){
        return "Error : Email must be a string"
    }
    if(!/[@]/.test(email)){
        return "Error : Email should containt '@' character"
    }

    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
        return "Valid"
    }else{
        return 'invalid'
    }
}

console.log(checkEmail("apranata@binar.co.id"))
console.log(checkEmail("apranata@binar.com"))
console.log(checkEmail("apranata@binar"))
console.log(checkEmail("apranata"))
console.log(checkEmail(666))
console.log(checkEmail())